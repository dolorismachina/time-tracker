﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Newtonsoft.Json;

namespace TimeTracker
{
    /// <summary>
    /// Interaction logic for TimeTrackerPage.xaml
    /// </summary>
    public partial class TimeTrackerPage : Page
    {
        private List<TrackedEvent> events = new List<TrackedEvent>();

        public TimeTrackerPage()
        {
            InitializeComponent();

            LoadEventsFromFile();
            Addshape();

        }

        private void SaveEventsToFile()
        {
            string serializedEvents = JsonConvert.SerializeObject(events, Formatting.Indented);
            StreamWriter sw = new StreamWriter("data.txt");
            sw.Write(serializedEvents);
            sw.Close();
        }

        private void LoadEventsFromFile()
        {
            try
            {
                StreamReader sr = new StreamReader("data.txt");
                string data = sr.ReadToEnd();
                sr.Close();
                
                if (data.Length > 0)
                {
                    events = JsonConvert.DeserializeObject<List<TrackedEvent>>(data);
                }
            }
            catch (FileNotFoundException e)
            {
                File.Create("data.txt");
            }

        }

        private static int CompareEventLengths(TrackedEvent x, TrackedEvent y)
        {
            if (x.DaysLeft > y.DaysLeft)
                return 1;
            else if (x.DaysLeft == y.DaysLeft)
                return 0;

            return -1;
        }

        int longest = 0;
        double heightPerDay = 0;

        private void Addshape()
        {
            events.Sort(CompareEventLengths);

            foreach (TrackedEvent e in events)
            {
                if (e.DaysLeft > longest)
                {
                    longest = e.DaysLeft;
                }
            }
            drawingCanvas.Children.Clear();
            heightPerDay = 200.0 / longest;
            for (int i = 0; i < events.Count; i++)
            {
                Rectangle rect = new Rectangle();
                SolidColorBrush brush = new SolidColorBrush(Color.FromRgb(100, 100, 255));
                rect.Fill = brush;
                rect.Width = 20;
                rect.Height = heightPerDay * events[i].DaysLeft;
                rect.Margin = new Thickness((i * 20) + (i * 5), 0, 0, 0); rect.ToolTip = events[i].Title;
                rect.RenderTransformOrigin = new Point(0.5, 0);
                ScaleTransform scale = new ScaleTransform(1, -1);
                TransformGroup group = new TransformGroup();
                group.Children.Add(scale);
                rect.RenderTransform = group;
                drawingCanvas.Children.Add(rect);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (eventTitle.Text != "" && eventDate.SelectedDate != null)
            {
                events.Add(new TrackedEvent(eventTitle.Text, eventDate.SelectedDate.Value.Date));
            }

            SaveEventsToFile();
            Addshape();
        }

    }

    public class TrackedEvent
    {
        public TrackedEvent()
        { }

        public TrackedEvent(string title, DateTime date)
        {
            this.Title = title;
            this.Date = date;
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        
        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set 
            {
                date = value;
                DaysLeft = (date - DateTime.Today).Days;
            }
        }

        private int daysLeft;
        public int DaysLeft
        {
            get { return daysLeft; }
            set { daysLeft = value; }
        }
    }
}
